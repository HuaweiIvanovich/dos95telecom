﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowTools : MonoBehaviour
{
	public bool isHighlighted;
	public GameObject ResizeH, ResizeV;
	public RectTransform ResizeH_RT, ResizeV_RT;

	
	void Start()
	{
		if (ResizeH != null && ResizeV != null)
		{
			isHighlighted =  (ResizeH.transform.parent == transform); //проверка на ком стоит tool
			
			ResizeH_RT = ResizeH.GetComponent<RectTransform>();
			ResizeV_RT = ResizeV.GetComponent<RectTransform>();
		}
	


    }

	public void PutOnMe()
    {
	    if (isHighlighted)
		    return;
    }
}
