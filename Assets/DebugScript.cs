﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugScript : MonoBehaviour
{
	public TextMeshProUGUI MouseCoordinatesX;
	public TextMeshProUGUI MouseCoordinatesY;


    void Update()
    {
	    Vector2 mousePos = Input.mousePosition;
	    MouseCoordinatesX.text = "X = " + mousePos.x.ToString("#.0");
	    MouseCoordinatesY.text = "Y = " + mousePos.y.ToString("#.0");

    }
}
