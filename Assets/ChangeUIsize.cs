﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;

public class ChangeUIsize : MonoBehaviour
{
	public enum Orientation : byte
	{
		Vertical = 0,
		Horizontal = 1
	}
	
	public RectTransform selfRT;
	public RectTransform motherObjectRT;
	public Orientation orientation;
	
	public float offset;
	
	public Text text;
	public Color textEnabled;
	public Color textDisabled;

	
    void Start()
	{
		selfRT = GetComponent<RectTransform>();
		SetToDefaultPosition();

		//rt.sizeDelta = new Vector2(100, 100);
		text.color = textDisabled;
	    
	}
    
	void SetToDefaultPosition()
	{
		if (orientation == Orientation.Horizontal)
			selfRT.anchoredPosition = new Vector2( motherObjectRT.sizeDelta.x + offset, - motherObjectRT.sizeDelta.y/2);
		if (orientation == Orientation.Vertical)
			selfRT.anchoredPosition = new Vector2( motherObjectRT.sizeDelta.x/2, -motherObjectRT.sizeDelta.y + offset);

	}

	public void OnDrag()
	{
		float x = motherObjectRT.sizeDelta.x;
		float y = motherObjectRT.sizeDelta.y;

		if (orientation == Orientation.Horizontal)
		{
			motherObjectRT.sizeDelta = new Vector2(selfRT.anchoredPosition.x - offset,y);
			text.text = selfRT.anchoredPosition.x.ToString("#.0");
		}
		if (orientation == Orientation.Vertical)
		{
			motherObjectRT.sizeDelta = new Vector2(x,-selfRT.anchoredPosition.y - offset);
			text.text = selfRT.anchoredPosition.y.ToString("#.0");
		}


	}
    
	[Button]
	public void OnDragBegin()
	{
		text.color = textEnabled;
	}
    
	[Button]
	public void OnDrop()
	{
		SetToDefaultPosition();
		text.color = textDisabled;
	}
}
