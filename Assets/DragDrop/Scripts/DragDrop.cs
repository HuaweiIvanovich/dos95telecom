﻿/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler {

    [SerializeField] private Canvas canvas;

    private RectTransform rectTransform;
	private CanvasGroup canvasGroup;
	public ChangeUIsize changeUIsize;
	
	Vector2 startDragPos;

    private void Awake() {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

	public void OnBeginDrag(PointerEventData eventData) {
    	
    	
        Debug.Log("OnBeginDrag");
        canvasGroup.alpha = .6f;
	    canvasGroup.blocksRaycasts = false;
	    changeUIsize.OnDragBegin();
    }

    public void OnDrag(PointerEventData eventData) {
        //Debug.Log("OnDrag");
	    rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
	    changeUIsize.OnDrag();

    }

    public void OnEndDrag(PointerEventData eventData) {
        Debug.Log("OnEndDrag");
        canvasGroup.alpha = 1f;
	    canvasGroup.blocksRaycasts = true;
	    changeUIsize.OnDrop();

    }

    public void OnPointerDown(PointerEventData eventData) {
        Debug.Log("OnPointerDown");
    }
    
	public void CanAddNewElement()
	{
		Vector2 mousePos = Input.mousePosition;
		if (mousePos.x < 159 &&
			mousePos.x > 48 &&
			mousePos.y <310 &&
			mousePos.y > 150)
			WaitSpawnNewElement();
			
	}
	
	public void WaitSpawnNewElement()
	{
		//if (!IsInvoking)
		//Invoke("SpawnNewElement", 1f);
	}
	
	public void SpawnNewElement()
	{
		//Instantiate
	}

}
